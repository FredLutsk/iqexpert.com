
var gulp            = require('gulp'),
sass            = require('gulp-sass'),
autoprefixer    = require('gulp-autoprefixer'),
sourcemaps      = require('gulp-sourcemaps'),
uglify          = require('gulp-uglify'),
rename          = require('gulp-rename'),
browserSync     = require('browser-sync'),
gulpImports     = require('gulp-imports'),
image           = require('gulp-image'),
minifyInline    = require('gulp-minify-inline'),
reload          = browserSync.reload;

var baseDir = "./app", 
    paths = {
        html:[ '*.html'],
        sass:[ baseDir + '/scss/**/*.scss'],
        script:[ baseDir + '/js/*.js']
};


gulp.task('html', function(){
    gulp.src(paths.html)
        .pipe(reload({stream:true}));
});



gulp.task('images', function(){
    gulp.src( baseDir + '/img/**/*')
        .pipe(image())
        .pipe(gulp.dest( baseDir + '/img-opt/'));
});


gulp.task('sass-prod', function() {
    return gulp.src([ baseDir + '/scss/main.scss'])
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(rename({suffix: '.min'}))
    .pipe(autoprefixer(['last 5 versions']))
    .pipe(minifyInline())
    .pipe(gulp.dest( baseDir + '/css'))
    .pipe(reload({stream: true}));
});

gulp.task('sass-dev', function() {
    return gulp.src([ baseDir + '/scss/main.scss'])
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(rename({suffix: '.min'}))
    .pipe(autoprefixer(['last 5 versions']))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest( baseDir + '/css'))
    .pipe(reload({stream: true}));
});


gulp.task('scripts', function() {
    return gulp.src([ baseDir + '/js/*.js'])
    .pipe(gulpImports())
    .pipe(uglify())
    .pipe(rename({suffix: ".min"}))
    .pipe(gulp.dest( baseDir + '/js/dist'))
    .pipe(reload({stream: true}));
});


gulp.task('browser-sync', function () {
    browserSync.init({
        proxy: 'http://IQexpert.test',
        host: 'IQexpert.test',
        open: 'external',
        port: 3000
    });
});


gulp.task('watch', function() {
    gulp.watch(paths.sass, ['sass-dev']);
    gulp.watch(paths.script, ['scripts']);
    // gulp.watch(paths.php, ['php']);
    gulp.watch(paths.html, ['html']);
});


gulp.task('dev', ['sass-dev', 'scripts', 'browser-sync', 'watch']);
gulp.task('prod', ['sass-prod', 'scripts']);
