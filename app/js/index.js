//html overflow hidden
var html = document.querySelector('html'),
    iw = window.innerWidth,
    ow = document.documentElement.clientWidth,
    padding = iw - ow;

function htmlScroll() {

    html.style.overflowY = 'hidden';
    html.style.paddingRight = padding + 'px';
}

function noHtmlScroll() {
    document.getElementsByTagName("html")[0].removeAttribute("style");
}


htmlScroll();

$(document).ready(function () {
    if ($(window).width() < 568) {
        var swiper = new Swiper('.js-features-slider', {
            slidesPerView: 'auto',
            spaceBetween: 60,
            pagination: {
                el: '.features__pagination',
                clickable: true,
            },
            containerModifierClass: 'features__body',
            wrapperClass: 'features__wrapper',
            slideClass: 'features__card',

        });
    }

    //hamburger
    $('.js-hamburger').click(function () {
        $(this).toggleClass('is-active')
        $(this).closest('.js-header-body').toggleClass('is-active')
    })

    //language
    $('.js-lang-item').click(function () {
        $(this).parent().toggleClass('is-active');
    })

    //cookie
    $('.js-accept, .js-decline').click(function () {
        $('.js-cookie-modal').fadeOut(function () {
            noHtmlScroll();
            $('html').attr('data-cookie', 'true');
        })
    })

    if ($(window).width() > 575) {
        AOS.init({
            once: true,
        });
    }

    //authorization
    $('.js-auth').click(function () {
        htmlScroll();
        $('#js-modal').fadeIn(function () {
            $('.js-hamburger').trigger('click');
        })
    })
    // var observer = lozad(); // lazy loads elements with default selector as '.lozad'
    // observer.observe();

    // var observer = lozad('.lozad', {
    // rootMargin: '500px 0px', // syntax similar to that of CSS Margin
    // threshold: 0.1 // ratio of element convergence
    // });
    // observer.observe();
    function frMslup() {
        $('#frMslup').find('span .slideUp').addClass('is-active');
        $('#frMslup').find('.js-main-sec-img').addClass('is-active');
    }
    frMslup();
    var rollAboutCondition = 1,
        rollFeaturesCondition = 1,
        rollHowCondition = 1;

    function rollAbout() {
        var topDiv = $(".js-about-pic").offset().top;
        var bottomDiv = topDiv + $(".js-about-pic").height();
        var viewPortSize = $(window).height();
        var pos = bottomDiv - viewPortSize


        if ($(window).scrollTop() >= pos) {
            $('.js-about-pic').addClass('visible');
            rollAboutCondition = 0;
        }
    }

    function rollFeatures() {
        var topDiv = $(".js-features-pic").offset().top;
        var bottomDiv = topDiv + $(".js-features-pic").height() / 2;
        var viewPortSize = $(window).height();
        var pos = bottomDiv - viewPortSize


        if ($(window).scrollTop() >= pos) {
            $('.js-features-pic').addClass('visible');
            rollFeaturesCondition = 0;
        }
    }

    function rollHow() {
        var topDiv = $(".js-left-wr").offset().top;
        var bottomDiv = topDiv + $(".js-left-wr").height() * .85;
        var viewPortSize = $(window).height();
        var pos = bottomDiv - viewPortSize


        if ($(window).scrollTop() >= pos) {
            $('html, body').animate({
                scrollTop: topDiv - 120
            }, 300, function () {
                $('.js-left-wr').addClass('animated');
                $('.js-how-card').addClass('animated');
                $('.js-left-inside-wr').addClass('animated');
                $('.js-how-body').addClass('animated');
                htmlScroll()
                setTimeout(function () {
                    $('.js-right-wr').addClass('animated');

                    $(".js-left-inside-wr").stick_in_parent({
                        // inner_scrolling: true
                        offset_top: 120,
                    });

                    if ($('html').data('cookie') === true) {
                        noHtmlScroll();
                    }
                    AOS.refresh({
                        once: true,
                    });
                    Waypoint.refreshAll();
                }, 700);
            });
            rollHowCondition = 0;

        }
    }

    $(window).scroll(function () {
        if (rollAboutCondition === 1) {
            rollAbout();
        }

        if (rollFeaturesCondition === 1 && $(window).width() > 799) {
            rollFeatures();
        }

        if (rollHowCondition === 1 && $(window).width() > 575) {
            rollHow()
        }


    });


    //like "x-ray" effect
    $('.js-about-txt').mousemove(function (e) {
        $(this).css({
            'background-position-x': e.offsetX - 35,
            'background-position-y': e.offsetY - 35
        })

    })
    $('.js-about-txt').mouseout(function (e) {
        $(this).css({
            'background-position-x': '-100px',
            'background-position-y': '-100px'
        })

    })


    if ($(window).width() > 575) {
        //binding "scroll down" label
        $('.js-how').on('mousemove', function (e) {

            $('.js-scroll-down').addClass('active').css({
                'transform': 'translate('+e.clientX+'px, '+e.clientY+'px)',
            })
        })
        $('.js-how').mouseout(function (e) {
            $('.js-scroll-down').removeClass('active').css({
                'transform': 'translateX(-100px)',
            })
        })


        //scroll mobile block
        var waypoints = $('.js-how-right').waypoint(function (direction) {
            if (direction === 'down') {
                var id = $(this.element).data('scroll');
                $('.js-how-left').removeClass('is-active');
                $('#id' + id).addClass('is-active');
            } else if (direction === 'up') {
                var id = $(this.element).data('scroll') - 1;
                id <= 0 ? id = 1 : id = id;
                $('.js-how-left').removeClass('is-active');
                $('#id' + id).addClass('is-active');
            }

            if (id === 5 && direction == 'down') {
                $('.js-scroll-down').fadeOut();
            } else {
                $('.js-scroll-down').fadeIn();
            }

        }, {
            offset: '50%'
        })
    }

    
   // grab an element
    var myElement = document.querySelector("header");
    // construct an instance of Headroom, passing the element
    var headroom  = new Headroom(myElement);
    // initialise
    headroom.init(); 

    //validation login form
    $('#login-form').validate({
        rules: {
            email: {
                required: true,
                email: true,
                minlength: 9,
            },
            pass: {
                required: true,
                minlength: 8,
            }
        },
        submitHandler: function (form) {
            console.log('add submit');

        }
    })

    //scroll mobile block
    var waypointsNav = $('section').waypoint(function (direction) {
        console.log(this.element);
        if (direction === 'down'){

            var href = $(this.element).attr('id');
            
            $('.js-nav').removeClass('is-active');
            $('.js-nav[href="'+href+'"]').addClass('is-active');
        }
        

    }, {
        offset: '50%'
    })

    $('.js-nav').click(function(e){
        e.preventDefault();
        var id = $(this).attr('href'),
            el = $('section'+id)
        $('html, body').animate({
            scrollTop: $(el).offset().top - 120
        }, 150)
    });

    $(document).click(function (e) {
        if(!$('.js-lang-wr').has(e.target).length){
            $('.js-lang-wr').removeClass('is-active')
        }
    })

})